﻿double Sum(double[] array)
{
    double sum = 0;
    foreach(var item in array)
    {
        sum += item;
    }
    return sum;
}

double Average(double[] array)
{
    double sum = Sum(array);
    return sum / array.Length;
}

double IndexOf(double[] array, double sought)
{
    int idx = -1;
    foreach (var item in array)
    {
        ++idx;
        if (item == sought)
            break;

    }
    return idx;
}

bool IsPositive(double[] array)
{
    foreach (var item in array)
        if (item <= 0)
            return false;
    return true;

}

bool HaveEqualLength(double[] a, double[] b)
{
    return a.Length == b.Length;
}

double[] RemoveElement(double[] array, double item)
{
    for(var i = 0; i < array.Length; ++i)
    {
        if (item == array[i])
        {
            var result = new double[array.Length - 1];

            for (var j = 0; j < i; ++j)
                result[j] = array[j];
            for (var j = i + 1; j < array.Length; ++j)
                result[j - 1] = array[j];
            return result;
        }
    }
    return array;
}

double Smallest(double[] array)
{
    if (array.Length == 0) throw new ArgumentOutOfRangeException("array was empty");
    var smallest = array[0];
    foreach(var item in array[1..])
    {
        if (item < smallest)
        {
            smallest = item;
        }
    }
    return smallest;
}

double[] Sort(double[] array)
{
    var sorted = new double[array.Length];
    var i = 0;

    while(array.Length != 0)
    {
        var smallest = Smallest(array);
        array = RemoveElement(array, smallest);
        sorted[i++] = smallest;
    }
    return sorted;
}
